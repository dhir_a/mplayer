package com.example.win_81.musicplayer;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends ListActivity {
    ArrayAdapter<String>adapter;
    File root;
    ArrayList<File>filelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_main);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1);
        root=new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        filelist=new ArrayList<File>();
        filelist=getfiles(root);
        for(int i=0;i<filelist.size();i++)
        {
            adapter.add(filelist.get(i).getName());
        }
        setListAdapter(adapter);
    }
   private ArrayList<File> getfiles(File root)
    {
        File list[]=root.listFiles();
        if(list!=null && list.length>0)
        {
            for(int i=0;i<list.length;i++)
            {
                if(list[i].isDirectory())
                {
                    getfiles(list[i]);
                }
                else
                {
                    if(list[i].getName().endsWith("mp3"))
                    {
                        filelist.add(list[i]);
                    }
                }
            }
        }
        return filelist;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent i=new Intent(getBaseContext(),Song.class);
        i.putExtra("my list",filelist);
        i.putExtra("position",position);
        startActivity(i);

    }
}
