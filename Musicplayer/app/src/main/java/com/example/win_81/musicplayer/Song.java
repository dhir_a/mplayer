package com.example.win_81.musicplayer;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


public class Song extends AppCompatActivity implements MediaPlayer.OnCompletionListener,SeekBar.OnSeekBarChangeListener {
    MediaPlayer mp;
    Button play, pause, stop, next, prev;
    ImageView view;
    ArrayList<File> files;
    TextView textView, currenttime, complitationtime;
    int pos;
    int k = 0;
    int length;
    long min, sec, millis1, millis2;
    boolean ispaused, t,isstop = false;
    SeekBar bar;
    Handler mHandler = new Handler();
    RelativeLayout layt;
    Random ra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);
        play = (Button) findViewById(R.id.button);
        pause = (Button) findViewById(R.id.button2);
        stop = (Button) findViewById(R.id.button3);
        next = (Button) findViewById(R.id.button4);
        prev = (Button) findViewById(R.id.button5);
        view = (ImageView) findViewById(R.id.imageView);
        textView = (TextView) findViewById(R.id.textView);
        currenttime = (TextView) findViewById(R.id.textView2);
        complitationtime = (TextView) findViewById(R.id.textView3);
        bar = (SeekBar) findViewById(R.id.seekBar);
        mp = new MediaPlayer();
         ra=new Random();
        layt=(RelativeLayout)findViewById(R.id.layer);
        layt.setBackgroundColor(Color.rgb(ra.nextInt(255),ra.nextInt(255),ra.nextInt(255)));
        files = (ArrayList<File>) getIntent().getSerializableExtra("my list");
        pos = getIntent().getIntExtra("position", 0);
        k = pos;
        textView.setText(files.get(k).getName());
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ispaused) {
                        mp.start();
                        mp.seekTo(length);
                        ispaused = false;
                    }
                    else {

                        if (mp.isPlaying()) {
                            mp.stop();
                            mp.release();
                        }
                        if(isstop)
                        {
                            mp.release();
                        }
                        mp = new MediaPlayer();
                        mp.setDataSource(files.get(k).getAbsolutePath());
                        mp.prepare();
                        mp.start();
                        bar.setMax(mp.getDuration());
                        millis2 = mp.getDuration();
                        millis1 = mp.getCurrentPosition();
                        min = (millis1 / 1000) / 60;
                        sec = (millis1 / 1000) % 60;
                        currenttime.setText(min + "." + sec);
                        min = (millis2 / 1000) / 60;
                        sec = (millis2 / 1000) % 60;
                        complitationtime.setText(min + "." + sec);
                        seekupdation();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mp.isPlaying()) {
                    mp.pause();
                    length = mp.getCurrentPosition();
                    ispaused = true;
                }

            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.removeCallbacks(r);
                mp.stop();
                isstop=true;
                //Intent j = new Intent(getBaseContext(), MainActivity.class);
                //startActivity(j);
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (k == (files.size() - 1)) {
                    k = 0;
                } else {
                    k = k + 1;
                }
                if (mp.isPlaying() || ispaused) {
                    mp.stop();
                    mp.release();

                }

                mp = new MediaPlayer();
                try {
                    mp.setDataSource(files.get(k).getAbsolutePath());
                    mp.prepare();
                    mp.start();
                    layt.setBackgroundColor(Color.rgb(ra.nextInt(255),ra.nextInt(255),ra.nextInt(255)));
                    bar.setMax(mp.getDuration());
                    millis2 = mp.getDuration();
                    millis1 = mp.getCurrentPosition();
                    min = (millis1 / 1000) / 60;
                    sec = (millis1 / 1000) % 60;
                    currenttime.setText(min + "." + sec);
                    min = (millis2 / 1000) / 60;
                    sec = (millis2 / 1000) % 60;
                    complitationtime.setText(min + "." + sec);
                    textView.setText(files.get(k).getName());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (k == 0) {
                    k = files.size() - 1;
                } else {
                    k = k - 1;
                }
                if (mp.isPlaying() || ispaused) {
                    mp.stop();
                    mp.release();

                }
                mp = new MediaPlayer();
                try {
                    mp.setDataSource(files.get(k).getAbsolutePath());
                    mp.prepare();
                    mp.start();
                    layt.setBackgroundColor(Color.rgb(ra.nextInt(255),ra.nextInt(255),ra.nextInt(255)));
                    bar.setMax(mp.getDuration());
                    millis2 = mp.getDuration();
                    millis1 = mp.getCurrentPosition();
                    min = (millis1 / 1000) / 60;
                    sec = (millis1 / 1000) % 60;
                    currenttime.setText(min + "." + sec);

                    min = (millis2 / 1000) / 60;
                    sec = (millis2 / 1000) % 60;
                    complitationtime.setText(min + "." + sec);
                    textView.setText(files.get(k).getName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        bar.setOnSeekBarChangeListener(this);

    }

    Runnable r = new Runnable() {
        @Override
        public void run() {
            seekupdation();

        }
    };

    public void seekupdation() {
        bar.setProgress(mp.getCurrentPosition());
        millis1 = mp.getCurrentPosition();
        min = (millis1 / 1000) / 60;
        sec = (millis1 / 1000) % 60;
        currenttime.setText(min + "." + sec);
        mHandler.postDelayed(r, 100);

    }

    @Override
    public void onBackPressed() {
        mHandler.removeCallbacks(r);
        mp.stop();
        mp.release();
        super.onBackPressed();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.release();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (mp != null && fromUser) {
            mp.seekTo(progress);
        }
    }

        @Override
        public void onStartTrackingTouch (SeekBar seekBar){

        }

        @Override
        public void onStopTrackingTouch (SeekBar seekBar){


        }
    }

